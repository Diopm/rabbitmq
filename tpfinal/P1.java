package tp2.rabbit.mq.prog;
import tp2.rabbit.mq.connexion.*;

import java.io.*;
import java.util.concurrent.TimeoutException;
import java.net.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.MessageProperties;
import com.rabbitmq.client.QueueingConsumer;

public class P1 {
	 static String message;
	  static String routingKey;
	// static ListLinks lk = new ListLinks();
	  private static void print(String msg, Object... args) {
	        System.out.println(String.format(msg, args));
	    }

	    private static String trim(String s, int width) {
	        if (s.length() > width)
	            return s.substring(0, width-1) + ".";
	        else
	            return s;
	    }
	public static void main(String[] argv) throws Exception,TimeoutException {
         DB db =new DB();
         RabbitConn rc = new RabbitConn();
		//String EXCHANGE_NAME = "echangeur_topic02";
		String Q1 = "Q1";
		String Q2 = "Q2";
		String Q3 = "Q3";
		String EXCHANGE="exchange";
		String cleHtml = "Page_HTML";
		String cleImage = "Image";
	
		boolean autoAck = false;
		boolean durable = true;
		boolean passive = true; // a true, on suppose que l'echangeur existe deja
		boolean autoDelete = false; // ne pas supprimer l'echangeur lorsqu'aucun client n'est connecte
		boolean exclusive = false;

	
		// recuperer le nom d file d'attente associee a  
	
		rc.canalDeCommunication.queueDeclare(Q1 , durable, exclusive, autoDelete, null);
		rc.canalDeCommunication.queueDeclare(Q2 , durable, exclusive, autoDelete, null);
		rc.canalDeCommunication.queueDeclare(Q3 , durable, exclusive, autoDelete, null);
		// lier la file d'attente a l'echangeur
		rc.canalDeCommunication.queueBind(Q2, EXCHANGE, cleHtml);
		rc.canalDeCommunication.queueBind(Q3, EXCHANGE, cleImage);
		// Ne pas delivrer a un consommateur plus qu'un message a la fois: Fair dispatch
		rc.canalDeCommunication.basicQos(1);
		
		System.out.println(" -* En attente de messages ... pour arreter pressez CTRL+C");

		QueueingConsumer buffer = new QueueingConsumer(rc.canalDeCommunication);
		rc.canalDeCommunication.basicConsume(Q1, autoAck, buffer);

		while (true) {
			QueueingConsumer.Delivery delivery = buffer.nextDelivery();
		message = new String(delivery.getBody());
	
			 print("Fetching %s...", message);
	          Document doc = Jsoup.connect(message).get();
	          String docHtml = Jsoup.connect(message).get().html(); //extraction du texte SAMI
	          Elements links = doc.select("a[href]");
	          Elements media = doc.select("[src]");
	          //P1 envoie les page html a Q2
	          
	          rc.canalDeCommunication.basicPublish(EXCHANGE,cleHtml, MessageProperties.PERSISTENT_TEXT_PLAIN,
	        		  docHtml.getBytes());
	      
	          System.out.println(" - Le message :'" + docHtml+ "' est envoye avec succes!");   
	          
	           
	           String imURL=null;
	           
	          // print("\nMedia: (%d)", media.size());
	          for (Element src : media) {
	              if (src.tagName().equals("img"))
	                 System.out.println(src.attr("abs:src"));
	              imURL = src.attr("abs:src");
	              byte[]images = getImage(imURL);
	              //Envoie
	              rc.canalDeCommunication.basicPublish(EXCHANGE,cleImage, MessageProperties.PERSISTENT_TEXT_PLAIN,
		        		  images);
	              System.out.println("Envoie réussi");
	              //channel.basicPublish("", NOM_FILE_DATTENTE, MessageProperties.PERSISTENT_BASIC, images);
	          }
            
	         // print("\nLinks: (%d)", links.size());
	          for (Element link : links) {
	             // print("  %s  ", link.attr("abs:href"), trim(link.text(), 35));
	             System.out.println(link.attr("abs:href"));
	              String url = link.attr("abs:href");
	              db.enregistreUrl(url);
	            /*
				   canalDeCommunication.basicPublish("", Q1, MessageProperties.PERSISTENT_TEXT_PLAIN,
							url.getBytes());
				   */
				   System.out.println(" - Le message :'" + url + "' est envoye avec succes!");  
	          }
		 
			/**********************/
	          
			/***********************/
			rc.canalDeCommunication.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
			
			
		}
		
		/****************************************************/

	}
         /*********************methode pour Extraction Image****************/
		
	public static byte[] getImage(String imgUrl) throws IOException {
		
		URL url = new URL(imgUrl);
		
		InputStream fis = url.openStream();
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		byte[] buf = new byte[1024];       
		try {
		for (int readNum; (readNum = fis.read(buf)) != -1;) {
		 bos.write(buf, 0, readNum); 
		}
		} catch (IOException ex) {
		     
		}
		byte[] bytes = bos.toByteArray();

		return bytes;

	}
  
			
}
