package tp2.rabbit.mq.prog;

import java.io.EOFException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.TimeoutException;
import tp2.rabbit.mq.connexion.*;

import com.rabbitmq.client.*;

public class P0 {
	 public static int nb;
	 private static String message;
	public static void main(String[] argv) throws java.io.IOException, SQLException,EOFException,NullPointerException, TimeoutException{
		RabbitConn rc = new RabbitConn();
		String Q1 = "Q1";//DB-->P0
	
		boolean durable = true;
		boolean autoAck = false;
		boolean passive = true; // a true, on suppose que l'echangeur existe deja
		boolean autoDelete = false; // ne pas supprimer l'echangeur lorsqu'aucun client n'est connecte
		boolean exclusive = false;
		// se connecter au broker RabbitMQ 
		//ConnectionFactory factory = new ConnectionFactory();

		// indiquer les parametres de la connexion
		/*
		factory.setUsername(nomUtilisateur);
		factory.setPassword(motDePasse);
		factory.setPort(numeroPort);
		factory.setVirtualHost(virtualHostName);
		factory.setHost(hostName);
        */

		// creer une nouvelle connexion
	//	Connection connexion;
		try {
		//	connexion = factory.newConnection();

			// ouvrir un canal de communication avec le Broker pour l'envoi et la reception de messages
			//Channel canalDeCommunication = connexion.createChannel();

			// declarer une file d'attente nommee NOM_FILE_DATTENTE
			rc.canalDeCommunication.queueDeclare(Q1 , durable, exclusive, autoDelete, null);

			// Ne pas delivrer a un consommateur plus qu'un message a la fois: Fair dispatch
			rc.canalDeCommunication.basicQos(1);
		 
			 ResultSet rs =null;
			    DB db = new DB();
			    String sql = "SELECT COUNT(url) FROM rabbit";
			    rs = db.runSql(sql);
			    while (rs.next()) {
					//On récupére le nombre d'enregistrement
					 
					 nb  = rs.getInt(1);	
				   }
			    for(int i=0;i<= nb;i++)
				{
					String sql2 = "select url from rabbit where idRabbit="+i+";";
					rs = db.runSql(sql2);
					
					while(rs.next())
					{
					   message = rs.getString("url");
					   rc.canalDeCommunication.basicPublish("", Q1, MessageProperties.PERSISTENT_TEXT_PLAIN,
								message.getBytes());
					   System.out.println(" - Le message :'" + message + "' est envoye avec succes!");   
					  
					}
				}
			    db.conn.close();
			rc.canalDeCommunication.close();

			// fermer la connexion
			//connexion.close();

		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
