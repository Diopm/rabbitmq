package tp2.rabbit.mq.prog;

import java.io.*;
import java.sql.*;
import tp2.rabbit.mq.connexion.*;
import java.net.*;
import com.rabbitmq.client.*;

import java.io.EOFException;
import java.sql.SQLException;
import java.util.concurrent.TimeoutException;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;


public class P3 {
	
	public static void main(String[] args) throws Exception{
		 DB db = new DB();
	     RabbitConn rc = new RabbitConn();
	        String Q3="Q3";
	        String EXCHANGE="exchange";
	        String cleImage = "Image";
	        boolean autoAck = false;
			boolean durable = true;
			boolean passive = true; // a true, on suppose que l'echangeur existe deja
			boolean autoDelete = false; // ne pas supprimer l'echangeur lorsqu'aucun client n'est connecte
			boolean exclusive = false;
			rc.canalDeCommunication.exchangeDeclare(EXCHANGE, "topic", passive, durable, autoDelete, null);
		    rc.canalDeCommunication.queueDeclare(Q3 , durable, exclusive, autoDelete, null);
		    rc.canalDeCommunication.queueBind(Q3, EXCHANGE, cleImage);
		    rc.canalDeCommunication.basicQos(1);
		    System.out.println(" -* En attente de messages ... pour arreter pressez CTRL+C");
		    QueueingConsumer buffer = new QueueingConsumer(rc.canalDeCommunication);
			rc.canalDeCommunication.basicConsume(Q3, autoAck, buffer);
	  
			 while (true) {
			    	QueueingConsumer.Delivery delivery = buffer.nextDelivery();
			    	 byte[]images  = delivery.getBody();
			    	System.out.println("Image recu" );			       
			    	db.sauveIMG(images);
			    	//sauveIMG(images,"name",conn);
			    	rc.canalDeCommunication.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
			    }
	
	}
}
