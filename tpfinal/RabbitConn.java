package tp2.rabbit.mq.connexion;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.QueueingConsumer;

public class RabbitConn {
	
	public Connection con = null;
	public Channel canalDeCommunication =null;
	public RabbitConn() throws IOException, TimeoutException,NullPointerException
	{
		
		
		String nomUtilisateur = "moussa"; 
		String motDePasse = "passer"; 
		int numeroPort = 5672; 
		String virtualHostName = "vhostPi"; 
		String hostName = "192.168.1.89";

		// se connecter au broker RabbitMQ
		ConnectionFactory factory = new ConnectionFactory();

		// indiquer les parametres de la connexion
		factory.setUsername(nomUtilisateur);
		factory.setPassword(motDePasse);
		factory.setPort(numeroPort);
		factory.setVirtualHost(virtualHostName);
		factory.setHost(hostName);

		// creer une nouvelle connexion
		Connection con = factory.newConnection();

		// ouvrir un canal de communication avec le Broker pour l'envoi et la
		// reception de messages
		 canalDeCommunication = con.createChannel(); 
		
		
	}
	public void fermer()
	{
		 try {
			canalDeCommunication.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

}
