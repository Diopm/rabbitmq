package tp2.rabbit.mq.connexion;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

 
public class DB {
 
	public Connection conn = null;
 
	public DB() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://192.168.1.94:3306/tp2Rabbitmq";
			conn = DriverManager.getConnection(url, "root", "fatma");
			System.out.println("connexion ok");
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
 
	public ResultSet runSql(String sql) throws SQLException {
		Statement sta = conn.createStatement();
		return sta.executeQuery(sql);
	}
 
	public boolean runSql2(String sql) throws SQLException {
		Statement sta = conn.createStatement();
		return sta.execute(sql);
	}
	public int runSql3(String sql) throws SQLException {
		Statement sta = conn.createStatement();
		return sta.executeUpdate(sql);
	}
 
	@Override
	protected void finalize() throws Throwable {
		if (conn != null || !conn.isClosed()) {
			conn.close();
		}
	}
          
	public  void sauveIMG(byte[] image) throws ClassNotFoundException,NullPointerException,SQLException,IOException
	  {   	    	
	      PreparedStatement ps =  conn.prepareStatement(
	          "insert into rabbit (image) values (?)",Statement.RETURN_GENERATED_KEYS);
	      try
	      {
	       // ps.setString(1,name);	      
	        ps.setBytes(1,image);
	        ps.execute();
	        
	      }
	      finally
	      {
	        ps.close();
	      }	  
	  }
        
          
	public void insertions(String texte)throws ClassNotFoundException,NullPointerException,SQLException,IOException{
		try
	      {
			 String query = " insert into rabbit (texte) values (?)";
			 // create the mysql insert preparedstatement
			PreparedStatement preparedStmt =conn.prepareStatement(query);
		    preparedStmt.setString (1, texte);
		 
		      // execute the preparedstatement
		      preparedStmt.execute();
		       System.err.println("save ok");
		     
		    }
		    catch (Exception e)
		    {
		      System.err.println("insertion échouée!");
		      System.err.println(e.getMessage());
		    }
		}
          
	public void enregistreUrl(String url) throws SQLException, IOException{
		//On vérifit si le lien existe dans la base de donnée
		String sql = "select * from rabbit where url = '"+url+"'";
		ResultSet rs = runSql(sql);
		if(rs.next()){
 
		}else{
			//store the URL to database to avoid parsing again
			sql = "INSERT INTO  `rabbit` " + "(`url`) VALUES " + "(?);";
			PreparedStatement stmt =conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, url);
			stmt.execute();			 
		}
	}

		   
}
