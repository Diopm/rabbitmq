package tp2.rabbit.mq.prog;

import java.io.*;
import java.sql.*;
import tp2.rabbit.mq.connexion.*;
import com.rabbitmq.client.*;
import java.io.EOFException;
import java.sql.SQLException;
import java.util.concurrent.TimeoutException;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;


public class P2 {
	public static void main(String[] argv) throws Exception,EOFException,SQLException,NullPointerException {
     DB db = new DB();
     RabbitConn rc = new RabbitConn();
        boolean autoAck = false;
		boolean durable = true;
		boolean passive = true; // a true, on suppose que l'echangeur existe deja
		boolean autoDelete = false; // ne pas supprimer l'echangeur lorsqu'aucun client n'est connecte
		boolean exclusive = false;
		String Q2 = "Q2";
		String EXCHANGE="exchange";
		String cleHtml = "Page_HTML";
	//	Channel canalDeCommunication = connexion.createChannel();
		rc.canalDeCommunication.exchangeDeclare(EXCHANGE, "topic", passive, durable, autoDelete, null);
	    rc.canalDeCommunication.queueDeclare(Q2 , durable, exclusive, autoDelete, null);
	    rc.canalDeCommunication.queueBind(Q2, EXCHANGE, cleHtml);
	    rc.canalDeCommunication.basicQos(1);
	    System.out.println(" -* En attente de messages ... pour arreter pressez CTRL+C");
	    QueueingConsumer buffer = new QueueingConsumer(rc.canalDeCommunication);
		rc.canalDeCommunication.basicConsume(Q2, autoAck, buffer);
		/**/
		String contenu;
      //  DB db = new DB();
     //   Statement st=null;
		while (true) {
			//Statement st=null;
			QueueingConsumer.Delivery delivery = buffer.nextDelivery();
			String message = new String(delivery.getBody());
			System.out.println(" - Le message :'" + message + "' recu");   
			//traitement de la page
			contenu = Jsoup.clean(message, Whitelist.basic());
			contenu=contenu.replaceAll("<[^>]*>"," ");
			//Insertion dans la BD
		  
			  
			/**********************insertion************DIOP****/
	    	db.insertions(contenu);
			rc.canalDeCommunication.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
			
		}
		/***************************************/
		 
		/**************************************/
		
	}
	
}
