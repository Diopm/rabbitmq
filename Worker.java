
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

public class Worker {
 // private final static String QUEUE_NAME = "hello";
  private static final String TASK_QUEUE_NAME ="hello";

  public static void main(String[] argv)
      throws java.io.IOException,java.lang.InterruptedException, TimeoutException {

    ConnectionFactory factory = new ConnectionFactory();
    factory.setHost("localhost");
    Connection connection = factory.newConnection();
    Channel channel = connection.createChannel();

    //channel.queueDeclare(QUEUE_NAME, false, false, false, null);
    
    System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
    
   // channel.basicQos(1);
    
    final Consumer consumer = new DefaultConsumer(channel) {
    	  @Override
    	  public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
    	    String message = new String(body, "UTF-8");

    	    System.out.println(" [x] Received '" + message + "'");
    	    try {
    	      doWork(message);
    	    } catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
    	      System.out.println(" [x] Done");
    	     // channel.basicAck(envelope.getDeliveryTag(), false);

    	    }
    	  }
    	};
    	try{
    	channel.basicConsume(TASK_QUEUE_NAME, true, consumer);
    	}catch(Exception e){}
    	System.out.println(consumer);
    
    }
    //on simule le temps d`éxécution de notre faux tache
  private static void doWork(String task) throws InterruptedException {
	    for (char ch: task.toCharArray()) {
	        if (ch == '.') Thread.sleep(1000);
	    }
	}
}