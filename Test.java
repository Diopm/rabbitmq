import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import java.util.concurrent.TimeoutException;
import com.rabbitmq.client.Channel;

public class Test {
  //on met en place le nom de la file d'attente
  private final static String QUEUE_NAME = "hello";

  public static void main(String[] argv)
      throws java.io.IOException {
   //connexion au serveur
    ConnectionFactory factory = new ConnectionFactory();
    factory.setHost("localhost");
    Connection connection = null;
  try {
    connection = factory.newConnection();
  } catch (TimeoutException e) {
    // TODO Auto-generated catch block
    e.printStackTrace();
  }
   //creation canal
    Channel channel = connection.createChannel();
 
    //Declaration de la file d'attente  
    channel.queueDeclare(QUEUE_NAME, false, false, false, null);
    //contenu du message qui est un tableau d'octet
    String message = "Hello World!";
    
    channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
    System.out.println(" [x] Sent '" + message + "'");
  //fermeture de la connexion
    try {
    channel.close();
  } catch (TimeoutException e) {
    // TODO Auto-generated catch block
    e.printStackTrace();
  }
    connection.close();
  }
}